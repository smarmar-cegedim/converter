package com.cegedim.next.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.io.File;
import java.io.IOException;

public class ApplicationMain implements CommandLineRunner {
    public static void main(final String[] args) throws IOException {
        SpringApplication.run(ApplicationMain.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var fileName = args[0];
        var objectMapper = new ObjectMapper();
        var xmlMapper = new XmlMapper();
        var node = objectMapper.readTree(new File(fileName));
        xmlMapper.writeTree(new XmlFactory().createGenerator(System.out), node);
    }
}
